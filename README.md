# Job Interview project

## Installation

```bash
git clone git@bitbucket.org:andreich1980/laravel-job-interview.git
# or
git clone https://andreich1980@bitbucket.org/andreich1980/laravel-job-interview.git

cd laravel-job-interview
composer install
cp .env.example .env
php artisan key:generate
touch database/database.sqlite
php artisan migrate --seed
npm install
npm run dev
php artisan serve
```
