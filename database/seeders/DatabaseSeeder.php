<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Login;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::factory(10)->has(Login::factory(100))->create();

        Category::factory(10)->has(Product::factory(100))->create();
    }
}
