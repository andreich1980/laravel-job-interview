<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function it_capitalizes_first_letter_of_the_first_and_last_name()
    {
        $user = User::factory()->make();

        $user->first_name = 'john';
        $user->last_name = 'doe';

        $this->assertEquals('John', $user->first_name);
        $this->assertEquals('Doe', $user->last_name);
    }

    /** @test */
    public function it_generates_full_name()
    {
        $user = User::factory()->make(['first_name' => 'John', 'last_name' => 'Doe']);

        $this->assertEquals('John Doe', $user->full_name);
    }
}
