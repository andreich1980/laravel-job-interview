<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    /** @test */
    public function marketingEmployeeEmails()
    {
        $employees = collect([
            ['name' => 'John', 'department' => 'Sales', 'email' => 'john3@example.com'],
            ['name' => 'Jane', 'department' => 'Marketing', 'email' => 'jane8@example.com'],
            ['name' => 'Dave', 'department' => 'Marketing', 'email' => 'dave1@example.com'],
            ['name' => 'Dana', 'department' => 'Engineering', 'email' => 'dana8@example.com'],
            ['name' => 'Beth', 'department' => 'Marketing', 'email' => 'beth4@example.com'],
            ['name' => 'Kyle', 'department' => 'Engineering', 'email' => 'kyle8@example.com'],
        ]);

        /*
         * Write a collection pipeline that returns just the email addresses
         * of every employee in the marketing department.
         *
         * Do not use any loops, if statements, or ternary operators.
         *
         * $emails = $employees->...
         */
        $emails = $employees;

        $this->assertEquals([
            'jane8@example.com',
            'dave1@example.com',
            'beth4@example.com',
        ], $emails->all());
    }

    /** @test */
    public function shoppingCartTotal()
    {
        $shoppingCart = collect([
            ['product' => 'Banana', 'unit_price' => 79, 'quantity' => 3],
            ['product' => 'Milk', 'unit_price' => 499, 'quantity' => 1],
            ['product' => 'Cream', 'unit_price' => 599, 'quantity' => 2],
            ['product' => 'Sugar', 'unit_price' => 249, 'quantity' => 1],
            ['product' => 'Apple', 'unit_price' => 76, 'quantity' => 6],
            ['product' => 'Bread', 'unit_price' => 229, 'quantity' => 2],
        ]);

        /*
         * Write a collection pipeline that calculates the total price of
         * all the items in this shopping cart.
         *
         * Do not use any loops, if statements, or ternary operators.
         *
         * $totalPrice = $shoppingCart->...
         */
        $totalPrice = 0;

        $this->assertEquals(3097, $totalPrice);
    }

    /** @test */
    public function employeesPerDepartment()
    {
        $employees = collect([
            ['name' => 'John', 'department' => 'Sales', 'email' => 'john3@example.com'],
            ['name' => 'Jane', 'department' => 'Marketing', 'email' => 'jane8@example.com'],
            ['name' => 'Dave', 'department' => 'Marketing', 'email' => 'dave1@example.com'],
            ['name' => 'Dana', 'department' => 'Engineering', 'email' => 'dana8@example.com'],
            ['name' => 'Beth', 'department' => 'Marketing', 'email' => 'beth4@example.com'],
            ['name' => 'Kyle', 'department' => 'Engineering', 'email' => 'kyle8@example.com'],
            ['name' => 'Steve', 'department' => 'Sales', 'email' => 'steve7@example.com'],
            ['name' => 'Liz', 'department' => 'Engineering', 'email' => 'liz6@example.com'],
            ['name' => 'Joe', 'department' => 'Marketing', 'email' => 'joe5@example.com'],
        ]);

        /*
         * Write a collection pipeline that returns an associative array that
         * shows how many employees are in each department.
         *
         * Do not use any loops, if statements, or ternary operators.
         *
         * $departmentCounts = $employees->...
         */
        $departmentCounts = collect();

        $this->assertEquals([
            'Sales' => 2,
            'Marketing' => 4,
            'Engineering' => 3,
        ], $departmentCounts->all());
    }

    /** @test */
    public function employeeWithMostValuableSale()
    {
        $employees = collect([
            [
                'name' => 'John',
                'email' => 'john3@example.com',
                'sales' => [
                    ['customer' => 'The Blue Rabbit Company', 'order_total' => 7444],
                    ['customer' => 'Black Melon', 'order_total' => 1445],
                    ['customer' => 'Foggy Toaster', 'order_total' => 700],
                ],
            ],
            [
                'name' => 'Jane',
                'email' => 'jane8@example.com',
                'sales' => [
                    ['customer' => 'The Grey Apple Company', 'order_total' => 203],
                    ['customer' => 'Yellow Cake', 'order_total' => 8730],
                    ['customer' => 'The Piping Bull Company', 'order_total' => 3337],
                    ['customer' => 'The Cloudy Dog Company', 'order_total' => 5310],
                ],
            ],
            [
                'name' => 'Dave',
                'email' => 'dave1@example.com',
                'sales' => [
                    ['customer' => 'The Acute Toaster Company', 'order_total' => 1091],
                    ['customer' => 'Green Mobile', 'order_total' => 2370],
                ],
            ],
        ]);

        /*
         * Using collection pipeline programming, find the employee who made
         * the most valuable sale.
         *
         * Do not use any loops, if statements, or ternary operators.
         *
         * $employeeWithMostValuableSale = $employees->...
         */
        $employeeWithMostValuableSale = $employees->first();

        $this->assertEquals('Jane', $employeeWithMostValuableSale['name']);
    }

    /** @test */
    public function mostValuableCustomer()
    {
        $employees = collect([
            [
                'name' => 'John',
                'email' => 'john3@example.com',
                'sales' => [
                    ['customer' => 'The Blue Rabbit Company', 'order_total' => 7444],
                    ['customer' => 'Black Melon', 'order_total' => 1445],
                    ['customer' => 'Yellow Cake', 'order_total' => 700],
                ],
            ],
            [
                'name' => 'Jane',
                'email' => 'jane8@example.com',
                'sales' => [
                    ['customer' => 'The Grey Apple Company', 'order_total' => 203],
                    ['customer' => 'Yellow Cake', 'order_total' => 8730],
                    ['customer' => 'The Blue Rabbit Company', 'order_total' => 3337],
                    ['customer' => 'Green Mobile', 'order_total' => 5310],
                ],
            ],
            [
                'name' => 'Dave',
                'email' => 'dave1@example.com',
                'sales' => [
                    ['customer' => 'The Acute Toaster Company', 'order_total' => 1091],
                    ['customer' => 'Green Mobile', 'order_total' => 2370],
                ],
            ],
            [
                'name' => 'Dana',
                'email' => 'dana2@example.com',
                'sales' => [
                    ['customer' => 'Green Mobile', 'order_total' => 203],
                    ['customer' => 'Yellow Cake', 'order_total' => 8730],
                    ['customer' => 'The Piping Bull Company', 'order_total' => 3337],
                    ['customer' => 'The Cloudy Dog Company', 'order_total' => 5310],
                ],
            ],
            [
                'name' => 'Beth',
                'email' => 'beth9@example.com',
                'sales' => [
                    ['customer' => 'The Grey Apple Company', 'order_total' => 1091],
                    ['customer' => 'Green Mobile', 'order_total' => 2370],
                ],
            ],
        ]);

        /*
         * Using collection pipeline programming, find the customer whose combined
         * total order value is the highest.
         *
         * Do not use any loops, if statements, or ternary operators.
         *
         * $mostValuableCustomer = $employees->...
         */
        $mostValuableCustomer = '';

        $this->assertEquals('Yellow Cake', $mostValuableCustomer);
    }
}
