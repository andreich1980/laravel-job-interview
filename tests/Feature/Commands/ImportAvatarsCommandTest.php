<?php

namespace Tests\Feature\Commands;

use App\Console\Commands\ImportAvatarsCommand;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ImportAvatarsCommandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers App\Console\Commands\ImportAvatarsCommand
    */
    public function it_fills_person_avatar_url()
    {
        User::factory(105)->create(['avatar_url' => null]);

        $this->artisan(ImportAvatarsCommand::class)->expectsOutput('Done: 105 avatars processed.');

        $this->assertDatabaseMissing('people', ['avatar_url' => null]);
    }
}
