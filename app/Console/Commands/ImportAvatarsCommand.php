<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class ImportAvatarsCommand extends Command
{
    protected $signature = 'import:avatars';

    protected $description = 'Import avatars';

    /**
     * @TODO: Refactor this method
     * @TODO: Add progress info (report once every 100 images processed)
     * @TODO: Make the test pass \Tests\Feature\Commands\ImportAvatarsCommandTest
     */
    public function handle()
    {
        $files = $this->getImages();

        foreach ($files as $filePath) {
            $parts = explode('/', $filePath);
            $parts = explode('.', end($parts));
            $id = $parts[0];
            $person = User::find($id);
            $person->storeAvatar($filePath);
        }

        return Command::SUCCESS;
    }

    /**
     * Get list of avatars from import folder.
     */
    private function getImages(): Collection
    {
        // We pretend that these are real files that should be processed.
        return User::all()->map(fn (User $user) => "images/{$user->id}.jpg");
    }
}
