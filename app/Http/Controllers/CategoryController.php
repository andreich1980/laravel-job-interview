<?php

namespace App\Http\Controllers;

use App\Models\Category;

class CategoryController extends Controller
{
    public function show(Category $category)
    {
//        $category->load('products.category');

        return view('categories.show', compact('category'));
    }
}
