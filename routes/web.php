<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LoginController;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logins', [LoginController::class, 'index']);

Route::get('/products/{category}/{product}', function (Category $category, Product $product) {
    return $product->name;
})->name('product');

Route::get('/categories/{category}', [CategoryController::class, 'show']);;

Route::view('counter', 'counter');
